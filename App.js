import React, { Component } from 'react';
import { Button, View, Text,AppRegistry } from 'react-native';
import SplashView from './components/SplashView';
import TabView from './components/TabView' ;



import { createStackNavigator, createAppContainer } from 'react-navigation';

const RootStack = createStackNavigator(
   {
      TabView: {
        screen: TabView,
      },
      SplashView: {
        screen: SplashView,
      },


  },
  {
    headerMode: 'none',
    initialRouteName: 'SplashView',
  },
);

const AppContainer = createAppContainer(RootStack);

export default class App extends React.Component {
  render() {
    return <AppContainer />;
  }
}


