import React from 'react';
import { View, Text ,Image} from 'react-native';

class SplashView extends React.Component {

  performTimeConsumingTask = async() => {
    return new Promise((resolve) =>
      setTimeout(
        () => { resolve('result') },
        2000
      )
    )
  }

  async componentDidMount() {

    const data = await this.performTimeConsumingTask();

    if (data !== null) {

      this.props.navigation.navigate('TabView');

     }

  }

  render() {
    return (
      <View style={styles.viewStyles}>

         <Image style={styles.image} source={{uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRX118hHOtPagvi8umls67zA7YfxEBfjM6mPiB9llD2HGkLChgSVg'}}/>

         <Text style={styles.textfield}> Consult top doctors from the best hospital  </Text>

      </View>
    );
  }
}

const styles = {
  viewStyles: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white'

  },
  image: {
    width:110,
    height:110,
    marginTop:30,
    borderRadius: 100 / 4,
  },
  textfield: {
     marginTop:30,
      fontSize: 20,
      color: "#000000",
      fontWeight: '500',
    },

}

export default SplashView ;