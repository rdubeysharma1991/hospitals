import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Alert,
  ScrollView,
  FlatList,
} from 'react-native';
import { withNavigation } from 'react-navigation';

class RoomNumbers extends Component {

  constructor(props) {
    super(props);
    this.state = {
      vipdata: [
        {id:1, RoomNo: "203",  Avalibility:'Yes' , amenities:'Television,Telephone,Locker,Slippers,Towels,Toiletries,WiFi'},
        {id:2, RoomNo: "204",  Avalibility:'Yes', amenities:'Television,Telephone,Locker,Slippers,Towels,Toiletries,WiFi'} ,
        {id:3, RoomNo: "205",  Avalibility:'No', amenities:'Television,Telephone,Locker,Slippers,Towels,Toiletries,WiFi'},
        {id:4, RoomNo: "206",  Avalibility:'Yes', amenities:'Television,Telephone,Locker,Slippers,Towels,Toiletries,WiFi'},
        {id:5, RoomNo: "207",  Avalibility:'Yes', amenities:'Television,Telephone,Locker,Slippers,Towels,Toiletries,WiFi'},
        {id:6, RoomNo: "208",  Avalibility:'No', amenities:'Television,Telephone,Locker,Slippers,Towels,Toiletries,WiFi'},
      ],
      SingleDuluxData: [
          {id:1, RoomNo: "103",  Avalibility:'Yes' , },
          {id:2, RoomNo: "104",  Avalibility:'Yes', } ,
          {id:3, RoomNo: "105",  Avalibility:'No', },
          {id:4, RoomNo: "106",  Avalibility:'Yes', },

      ],
      fourBeddedData:[
         {id:1, RoomNo: "10",  Avalibility:'Yes' , },
      ] ,
      twoBeddedData:[
           {id:1, RoomNo: "9",  Avalibility:'Yes' , },
        ]

    };
  }


  render() {
    return (
      <View style={styles.container}>
        <FlatList style={styles.list}
          contentContainerStyle={styles.listContainer}
          data={this.state.vipdata}
          horizontal={false}
          numColumns={1}
          keyExtractor= {(item) => {
            return item.id;
          }}
          ItemSeparatorComponent={() => {
            return (
              <View style={styles.separator}/>
            )
          }}
          renderItem={(post) => {
            const item = post.item;
            return (
              <View style={styles.card}>
                <View style={styles.cardContent}>
                  <Text style={styles.title}>Room no. : {item.RoomNo}</Text>
                   <Text style={styles.Avalibility}>Avalibility : {item.Avalibility}</Text>
                  <Text style={styles.amenities}>Amenities : {item.amenities}</Text>

                </View>
              </View>
            )
          }}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    marginTop:20,
  },
  list: {
    paddingHorizontal: 10,
  },
  listContainer:{
    alignItems:'center'
  },
  separator: {
    marginTop: 10,
  },
  /******** card **************/

   card:{
     shadowColor: '#00000021',
     shadowOffset: {
       width: 0,
       height: 6,
     },
     shadowOpacity: 0.37,
     shadowRadius: 7.49,
     elevation: 12,
     marginLeft: 10,
     marginRight: 10,
     marginTop:10,
     backgroundColor:"white",
     padding: 10,
     flexDirection:'row',
     borderRadius:6,
   },
  cardContent: {
    paddingVertical: 17,
    justifyContent: 'space-between',
  },


  /******** card components **************/
  title:{
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize:18,
    flex:1,
    color:"#000",

  },
  Avalibility:{
      textAlign: 'center',
      fontWeight: 'bold',
      fontSize:14,
      flex:1,
      color:"#006400",

    },
  amenities:{
    fontSize:12,
    flex:1,
    color:"#0078D7"
  },
});


  export default withNavigation(RoomNumbers);