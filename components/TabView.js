import React, { Component } from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import {StyleSheet, Text, View} from 'react-native';
import { createBottomTabNavigator, createAppContainer,createStackNavigator } from 'react-navigation';
import { withNavigation } from 'react-navigation';

import HospitalProfile from './HospitalProfile';
import RoomType from './RoomType';
import PatientsDetail from './PatientsDetail';
import RoomNumbers from './RoomNumbers' ;
import PatientProfile from './PatientProfile' ;

class TabView extends Component {

    render() {

        const RootStack = createBottomTabNavigator(

             {
               HospitalProfile:{
                screen:HospitalProfile,
                navigationOptions:{
                  tabBarLabel:'About us',
                  tabBarIcon:({tintColor})=>(
                      <Icon name="ios-home" color={tintColor} size={25}/>
                  )
                }
              },
              RoomType:{
                      screen:RoomType,
                      navigationOptions:{
                        tabBarLabel:'Rooms',
                        tabBarIcon:({tintColor})=>(
                            <Icon name="ios-more" color={tintColor} size={25}/>
                        )
                      }
              },
              PatientsDetail: {
                screen:PatientsDetail,
                navigationOptions:{
                  tabBarLabel:'Patients Detail',
                  tabBarIcon:({tintColor})=>(
                      <Icon name="ios-person" color={tintColor} size={25}/>
                  )
                }
              },
            },
            {
              initialRouteName: "HospitalProfile"
            },
        );



        const styles = StyleSheet.create({
              container: {
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center'
              },
            });



           const PrimaryNav = createStackNavigator({

                 RootStack:{
                       screen: RootStack,
                        },
                 RoomNumbers:{
                        screen: RoomNumbers,
                       },
                  PatientProfile:{
                        screen: PatientProfile,
                       },


            },
            {
                headerMode: 'none',

            },

          )

        const AppContainer = createAppContainer(PrimaryNav);

        return <AppContainer />;
    }

  }


  export default withNavigation(TabView);





