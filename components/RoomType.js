import React, { Component } from 'react';
import { createStackNavigator, createAppContainer } from 'react-navigation';

import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Alert,
  ScrollView,
  FlatList,
} from 'react-native';
import { withNavigation } from 'react-navigation';


class RoomType extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data: [
        {id:1, title: "VIP-Suite",  count:4, image:"http://www.regencyspecialist.com/wp-content/uploads/2012/09/VIP-Suite.jpg"},
        {id:2, title: "Single-Dulux-Room ",  count:4, image:"http://www.regencyspecialist.com/wp-content/uploads/2012/09/Single-Dulux-Room.jpg"} ,
        {id:3, title: "Two-Bedded-Room",  count:4, image:"http://www.regencyspecialist.com/wp-content/uploads/2012/09/Two-Bedded-Room.jpg"},
        {id:4, title: "4-Bedded-Room",  count:4, image:"http://www.regencyspecialist.com/wp-content/uploads/2012/09/4-Bedded-Room.jpg"},
        {id:5, title: "c-Room",  count:4, image:"http://santiamhospital.org/_images/ICU_Patient-Room.jpg"},
        {id:6, title: "Isoltion-Room",  count:4, image:"https://media.winnipegfreepress.com/images/141020-_EBOLA_01_18036667.jpg"},
      ]
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <FlatList style={styles.list}
          contentContainerStyle={styles.listContainer}
          data={this.state.data}
          horizontal={false}
          numColumns={2}
          keyExtractor= {(item) => {
            return item.id;
          }}
          ItemSeparatorComponent={() => {
            return (
              <View style={styles.separator}/>
            )
          }}
          renderItem={(post) => {
            const item = post.item;
            return (
              <View style={styles.card}>

                <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('RoomNumbers')  } >
                 <View style={styles.imageContainer}>
                  <Image style={styles.cardImage} source={{uri:item.image}}/>
                  <Text style={styles.title}>{item.title}</Text>
                 </View>
                </TouchableOpacity>

              </View>
            )
          }}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    marginTop:20,
  },
  list: {
    paddingHorizontal: 10,
  },
  listContainer:{
    alignItems:'center'
  },
  separator: {
    marginTop: 10,
  },
  /******** card **************/
  card:{
    marginVertical: 8,
    backgroundColor:"white",
    flexBasis: '45%',
    marginHorizontal: 10,
    shadowColor: '#00000021',
       shadowOffset: {
         width: 0,
         height: 6,
       },
       shadowOpacity: 0.37,
       shadowRadius: 7.49,
       elevation: 12,
        backgroundColor:"white",
                          padding: 10,
                          flexDirection:'row',
                          borderRadius:6,
  },


  cardContent: {
    paddingVertical: 17,
    justifyContent: 'space-between',
  },
  cardImage:{
    flex: 1,
    height: 150,
    width: 150,
  },
  imageContainer:{
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.32,
    shadowRadius: 5.46,

    elevation: 9,
  },
  /******** card components **************/
  title:{
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize:12,
    flex:1,
    color:"#000",
    marginTop:10,

  },
  count:{
    fontSize:18,
    flex:1,
    color:"#B0C4DE"
  },
});

  export default withNavigation(RoomType);

