import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import { withNavigation } from 'react-navigation';

class PatientProfile extends Component {

  constructor(props) {

    super(props);

    this.state = {

      patientName: '',
      patientImage:'',
      dept:'',
      patientAddress:'',
      maritalstatus:'',

    }

        patientName = this.props.navigation.state.params.patientName ;
        patientImage = this.props.navigation.state.params.patientImage ;
        patientDept = this.props.navigation.state.params.patientDept;
        patientAddress = this.props.navigation.state.params.patientAddress ;
        maritalstatus = this.props.navigation.state.params.maritalstatus;



  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <View style={styles.headerContent}>
            <Image style={styles.avatar}
              source={{ uri: patientImage }} />

          </View>
          <Text style={styles.name1}>Patient Information </Text>
        </View>

        <View style={styles.body}>


          <View style={styles.inputContainer}>

             <Text style={styles.name}>Name : {patientName}</Text>

          </View>

          <View style={styles.inputContainer}>

           <Text style={styles.name}>Department: {patientDept}</Text>

          </View>

          <View style={styles.inputContainer}>

            <Text style={styles.name}>Address : {patientAddress} </Text>

          </View>

          <View style={styles.inputContainer}>

           <Text style={styles.name}>Contact : +965445</Text>

          </View>

           <View style={styles.inputContainer}>

              <Text style={styles.name}>Religion : Hindu</Text>

            </View>


             <View style={styles.inputContainer}>

                <Text style={styles.name}>Status : {maritalstatus} </Text>

              </View>

        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#DCDCDC',
    padding:30

  },
  header: {
    backgroundColor: "#DCDCDC",
    marginTop:20,
  },
  headerContent: {
    alignItems: 'center',
    marginTop:60,
  },
  avatar: {
    width: 100,
    height: 100,
    borderRadius: 63,
    borderWidth: 4,
    borderColor: "white",
    marginBottom: 10,
  },
  name: {
   marginTop:15,
    marginLeft:20,
    fontSize: 16,
    color: "#000000",
    fontWeight: '100',
  },
  name1: {
    fontSize: 18,
    color: "#000000",
    fontWeight: '400',
  },
  userInfo: {
    fontSize: 16,
    color: "#778899",
    fontWeight: '600',
  },
  body: {
    backgroundColor: "#DCDCDC",
    height: 500,
    alignItems: 'center',

  },
  item: {
    flexDirection: 'row',
  },
  infoContent: {
    flex: 1,
    alignItems: 'flex-start',
    paddingLeft: 5
  },
  iconContent: {
    flex: 1,
    alignItems: 'flex-end',
    paddingRight: 5,
  },
  icon: {
    width: 30,
    height: 30,
    marginTop: 20,
  },
  info: {
    fontSize: 18,
    marginTop: 20,
    color: "#FFFFFF",
  },
  inputContainer: {
    borderBottomColor: '#F5FCFF',
    backgroundColor: '#FFFFFF',
    borderBottomWidth: 1,
    width: 400,
    height: 60,
    marginTop: 10,
    flexDirection: 'row',
    shadowColor: "#808080",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.10,
    shadowRadius: 1.15,

    elevation: 1,
  },


});


export default withNavigation(PatientProfile);