import React, { Component } from 'react';
import { AppRegistry, View,StyleSheet,Text,ListView, TextInput, ActivityIndicator, Alert,TouchableOpacity,Image,FlatList,ScrollView,Dimensions} from 'react-native';
import ScrollableTabView, { DefaultTabBar, ScrollableTabBar} from 'react-native-scrollable-tab-view-forked'

export default class CouponTab extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      modalVisible:false,
      userSelected:[],
      data: [
        {id:1,  name: "Joe joseph",        image:"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQTFW8QfIHziaUxzqF6Eclnvpt85lmKELV38TKCs6lanZtJUUA4",   dept:'Neurosurgeon', doctor:'Vivek sharma' , address :'345, Plasia ,Indore' , maritalstatus:'Single'},
        {id:2,  name: "Nick rodrigues",    image:"https://png.pngtree.com/png-clipart/20190515/original/pngtree-businessman-and-intellectual-character-png-image_3741369.jpg",  dept:'ENT' ,     doctor:'Saloni tripathi'  , address :'chennai' , maritalstatus:'Single'},
        {id:3,  name: "Rafel nadal",       image:"https://png.pngtree.com/png-clipart/20190516/original/pngtree-vector-pop-art-illustration-of-a-brutal-bearded-man-macho-with-png-image_3572220.jpg", dept:'Orthopaedic' ,doctor:'AL sharma'  , address :'543, Vijay chowk, Gwalior' , maritalstatus:'Married'} ,
        {id:4,  name: "Priyanka chopra",   image:"https://png.pngtree.com/png-clipart/20190515/original/pngtree-vector-pop-art-surprised-girl-png-image_3567980.jpg", dept: 'Neurosurgeon',   doctor:'Niranjan das'  , address :'Chinwada' , maritalstatus:'Single'} ,
        {id:5,  name: "Deepika padukon",   image:"https://png.pngtree.com/png-clipart/20190515/original/pngtree-vector-pop-art-surprised-girl-png-image_3567980.jpg", dept: 'ENT',       doctor:'RK Gopalan'  , address :'Pune' , maritalstatus:'Single'} ,
        {id:6,  name: "Lashkar singh",     image:"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQmMyfRCGlBdIv3SH0duNeKJQ1oHTPlS488GWB7S_dmfTkfb7xx",dept: 'ENT',        doctor:'Nidhi kapoor'  , address :'Mumbai' , maritalstatus:'Married'} ,
        {id:7,  name: "Tanveer",           image:"https://s3.amazonaws.com/pix.iemoji.com/images/emoji/apple/ios-12/256/man-wearing-turban-medium-dark-skin-tone.png",  dept:'General physician' ,      doctor:'Ritu chandelwal'  , address :'Indore' , maritalstatus:'Married'} ,


      ]
    };
  }

  clickEventListener = (item) => {

//    Alert.alert('Message', 'Item clicked. '+item.name);

    console.log(item.name)

    this.props.navigation.navigate('PatientProfile', {
                                                patientImage: item.image,
                                                patientName:item.name,
                                                patientDept:item.dept,
                                                patientAddress:item.address,
                                                maritalstatus:item.maritalstatus,
                                                              })
}

  render() {
    return (

    <View style={styles.MainContainer}>
              <FlatList
                         style={styles.contentList}
                         columnWrapperStyle={styles.listContainer}
                         data={this.state.data}
                         keyExtractor= {(item) => {
                           return item.id;
                         }}
                         renderItem={({item}) => {
                         return (
                           <TouchableOpacity style={styles.card} onPress={() => {this.clickEventListener(item)}}>
                             <Image style={styles.image} source={{uri: item.image}}/>
                             <View style={styles.cardContent}>

                                <Text style={styles.name}>Patient: {item.name}</Text>
                                <Text style={styles.doctor}> Doctor: {item.doctor}</Text>
                                 <Text style={styles.department}> Dept: {item.dept} </Text>
                             </View>

                           </TouchableOpacity>
               )}}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({

  MainContainer :{
    justifyContent: 'center',
    flex:1,
    margin: 7,
    },


    container:{
       flex:1,
       marginTop:60,
       backgroundColor:"#ebf0f7"
     },
     contentList:{
       flex:1,
     },
     cardContent: {
       marginLeft:10,
       marginTop:5
     },
      cardContentDoctor: {
                    marginLeft:20,

          },
     image:{
       width:90,
       height:90,
       borderRadius:2,
       borderWidth:2,
       borderColor:"#ebf0f7"
     },

     card:{
       shadowColor: '#00000021',
       shadowOffset: {
         width: 0,
         height: 6,
       },
       shadowOpacity: 0.37,
       shadowRadius: 7.49,
       elevation: 12,
       marginLeft: 10,
       marginRight: 10,
       marginTop:10,
       backgroundColor:"white",
       padding: 10,
       flexDirection:'row',
       borderRadius:6,
     },

     name:{
       fontSize:14,
       flex:1,
       color:"#483d8b",
       fontWeight:'bold'
     },
      doctor:{
      marginTop:10,
        fontSize:14,
        flex:1,
        color:"#483d8b",

                  },

     department:{
            color: "#000000",
            fontSize:12,
          },
       container1: {
                 position: 'absolute',
                 bottom:0,
                 marginTop:20,
              },
    inputContainer: {
           borderBottomColor: '#F5FCFF',
           backgroundColor: '#FFFFFF',
           borderRadius:7,
           borderBottomWidth: 1,
           height:45,
           marginBottom:20,
           flexDirection: 'row',
           alignItems:'center',
           marginTop:20,

           shadowColor: "#808080",
           shadowOffset: {
             width: 0,
             height: 2,
           },
           shadowOpacity: 0.25,
           shadowRadius: 3.84,

           elevation: 5,
         },
         inputs:{
           textAlign: 'center',
           height:45,
           marginLeft:16,
           borderBottomColor: '#FFFFFF',
           flex:1,
         },
         inputIcon:{
           width:15,
           height:15,
           marginRight:15,
           justifyContent: 'center',
           position: 'absolute',
           right: 5,
         },
           inputIconMap:{
            width:15,
            height:15,
            marginLeft:15,
            justifyContent: 'center',
            position: 'absolute',
            left: 5,
          },
});
