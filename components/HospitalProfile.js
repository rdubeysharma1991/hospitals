import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  ImageBackground,
  ScrollView,
} from 'react-native';
import { withNavigation } from 'react-navigation';

class HospitalProfile extends Component {

  constructor(props) {

    super(props);

    this.state = {

      user_nicename: '',
      user_email: '',

    }
  }

  render() {
    return (

    <ScrollView>
      <View style={styles.container}>

              <ImageBackground
                      source={{ uri: 'http://new.swadesfoundation.org/wp-content/uploads/2018/07/fortis-hospitals-logo-A9EB82FFBA-seeklogo.com_-300x179.png' }}
                      style={{
                        height: 250,
                        width: 400,
                        padding:5,
                 }}/>

              <Text style={styles.name}> Fortis </Text>

            <Text style={styles.name1}>Services</Text>

            <View style={styles.body}>

                  <View style={styles.inputContainer}>

                       <Text style={styles.services}> * Consultant Out-Patient Clinics  </Text>

                       <Text style={styles.services}> * Diagnostic Radiological and Laboratory services </Text>

                       <Text style={styles.services}> * Day Surgery  </Text>

                       <Text style={styles.services}> * Clinical Nurse Specialists in the areas of Diabetes, Continence Management, Respiratory Care, Health Promotion, Infection Control, Palliative Care and Pain Management   </Text>

                       <Text style={styles.services}> * A dietetic service providing nutritional assessment, dietary advice, education and evaluation  </Text>

                   </View>

            </View>
      </View>
     </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#DCDCDC',
    padding:10

  },
  header: {
    backgroundColor: "#DCDCDC",
  },
  headerContent: {
    alignItems: 'center',
  },
  avatar: {
    width: 100,
    height: 100,
    borderRadius: 63,
    borderWidth: 4,
    borderColor: "white",
    marginBottom: 10,
  },
  name: {
    fontSize: 20,
    color: "#000000",
    fontWeight: '600',
    marginTop:10,
  },
  name1: {
     marginTop:20,
    fontSize: 17,
    color: "#000000",
    fontWeight: '300',
  },
   services: {
     marginLeft :10,
     marginRight:10,
      fontSize: 16,
      color: "#000000",
      fontWeight: '300',
     textAlign: 'justify',
     lineHeight: 40,
    },
  userInfo: {
    fontSize: 16,
    color: "#778899",
    fontWeight: '600',
  },
  body: {
    backgroundColor: "#DCDCDC",
    height: 500,
    alignItems: 'center',

  },
  item: {
    flexDirection: 'row',
  },
  infoContent: {
    flex: 1,
    alignItems: 'flex-start',
    paddingLeft: 5
  },
  iconContent: {
    flex: 1,
    alignItems: 'flex-end',
    paddingRight: 5,
  },
  icon: {
    width: 30,
    height: 30,
    marginTop: 20,
  },
  info: {
    fontSize: 18,
    marginTop: 20,
    color: "#FFFFFF",
  },
  inputContainer: {
    borderBottomColor: '#F5FCFF',
    backgroundColor: '#FFFFFF',
    borderBottomWidth: 1,
    width: 400,
    marginTop: 8,
    shadowColor: "#808080",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.10,
    shadowRadius: 1.15,
    elevation: 1,
  },
  inputs: {
    height: 45,
    marginLeft: 20,
    borderBottomColor: '#FFFFFF',
    flex: 1,
  },
  buttonContainer: {
    height: 45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
    width: 400,

    backgroundColor: 'transparent'
  },
  loginButton: {
    backgroundColor: "#421a8d",

    shadowColor: "#421a8d",
    shadowOffset: {
      width: 0,
      height: 9,
    },
    shadowOpacity: 0.50,
    shadowRadius: 12.35,

    elevation: 3,
  },
  loginText: {
    color: 'white',
  },
});


export default withNavigation(HospitalProfile);